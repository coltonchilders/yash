#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


#define EXIT_SUCCESS 0

char cmd_line[2000];
//char* args[100];

typedef struct process
{
  int pid;
  char** args;
  int file_in;
  int file_out;
  int error;
  struct process* next_process;
} process;

typedef struct job
{
  int pgid;
  process* start_process;
  int pipefd[2];
  struct job* next_job;
} job; 

void parse_cmd2(job* j, char* prev_token)
{
  process* p = malloc(sizeof(process));
  p->args = malloc(sizeof(100));
  p->file_in = j->pipefd[0];
  p->file_out = -1;
  p->error = -1;
  
  j->start_process->next_process = p;

  char* token = prev_token;
  //printf("%s\n", token);//////////////////////////////////debug
  int i = 0;
  p->args[i] = token;
  token = strtok(NULL, " \t\n");
  i++;
  while (token != NULL)
  {
    if(strcmp(p->args[i - 1], ">") == 0)
    {
      p->file_out = open(token, O_RDWR | O_CREAT);
      if(p->file_out == -1)
      {
        printf("ERROR: FILE DOES NOT EXIST\n");
      }
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i - 1], "2>") == 0)
    {
      p->error = open(token, O_CREAT);
      if(p->error == -1)
      {
        printf("ERROR: FILE DOES NOT EXIST\n");
      }
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i -1], "2>&1") == 0)
    {
      p->error = STDOUT_FILENO;
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i - 1], "|") == 0)
    {
      printf("ERROR: TOO MANY PIPES. I'M NOT THAT SOPHISTICATED\n");
      /*if(pipe(j->pipefd) == -1)
      {
        printf("ERROR: PIPE FAIL: %d\n", errno);
        _exit(1);
      }
      p->file_out = j->pipefd[1];
      parse_cmd2(j);*/
    }
    else
    {
      p->args[i] = token;
      token = strtok(NULL, " \t\n");
      i++;
    }
  }
  return;
}

job* parse_cmd()
{
  process* p = malloc(sizeof(process));
  p->args = malloc(sizeof(100));
  p->file_in = -1;
  p->file_out = -1;
  p->error = -1;
  
  job* j = malloc(sizeof(job));
  j->start_process = p;


  char* token = strtok(cmd_line, " \t\n");
  //printf("%s\n", token);//////////////////////////////////debug
  int i = 0;
  p->args[i] = token;
  token = strtok(NULL, " \t\n");
  //printf("%s\n", token);//////////////////////////////////debug
  i++;
  while (token != NULL)
  {
    if(strcmp(p->args[i - 1], "<") == 0)
    {
      p->file_in = open(token, O_RDONLY);
      if(p->file_in == -1)
      {
        printf("ERROR: FILE DOES NOT EXIST\n");
      }
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i - 1], ">") == 0)
    {
      p->file_out = open(token, O_RDWR | O_CREAT);
      if(p->file_out == -1)
      {
        printf("ERROR: FILE DOES NOT EXIST\n");
      }
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i - 1], "2>") == 0)
    {
      p->error = open(token, O_CREAT);
      if(p->error == -1)
      {
        printf("ERROR: FILE DOES NOT EXIST\n");
      }
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i -1], "2>&1") == 0)
    {
      p->error = STDOUT_FILENO;
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else if(strcmp(p->args[i - 1], "|") == 0)
    {
      if(pipe(j->pipefd) == -1)
      {
        printf("ERROR: PIPE FAIL: %d\n", errno);
        _exit(1);
      }
      p->file_out = j->pipefd[1];
      parse_cmd2(j, token);
      p->args[i - 1] = NULL;
      token = NULL;
    }
    else
    {
      p->args[i] = token;
      token = strtok(NULL, " \t\n");
      //printf("%s\n", token);//////////////////////////////////debug
      i++;
    }
  }
  return j;
}


void run(job* j)
{
  process* p;
  for(p = j->start_process; p != NULL; p = p->next_process)
  {
    int status;

    p->pid = fork();
    //printf("For begin... pid: %d, status: %d\n", p->pid, status);//////////////////////////////////debug
    if(p->pid < 0) { _exit(1); }
    else if(p->pid == 0) //child process
    {

      //signal (SIGQUIT, SIG_DFL);
      //signal (SIGTTIN, SIG_IGN);
      //signal (SIGTTOU, SIG_IGN);
      //signal (SIGCHLD, yash_sigchld_handler);
      //signal (SIGTSTP, yash_sigtstp_handler);
      //signal (SIGINT, yash_sigint_handler);

      if(p->file_in != -1)
      {
        if(dup2(p->file_in, STDIN_FILENO) < 0) { printf("ERROR: DESCRIPTOR FILE_IN NOT DUPPED\n"); }
        close(p->file_in);
      }
      if(p->file_out != -1)
      {
        if(dup2(p->file_out, STDOUT_FILENO) < 0) { printf("ERROR: DESCRIPTOR FILE_OUT NOT DUPPED\n"); }
        close(p->file_out);
      }
      if(p->error != -1)
      {
        if(dup2(p->error, STDERR_FILENO) < 0) { printf("ERROR: DESCRIPTOR ERROR NOT DUPPED\n"); }
        close(p->error);
      }

      //printf("Process running: %d\n", p->pid);//////////////////////////////////debug
      execvp(p->args[0], p->args);

      printf("ERROR: EXEC FAIL: %d\n", errno);
    
    }
    else //parents process
    { 
      //printf("Parent process\n");//////////////////////////////////debug
      //signal(SIGTSTP, yash_sigtstp_handler);
      waitpid(p->pid, &status, 0); 
      if(p->file_out != -1) { close(p->file_out); }
      if(p->file_in != -1) { close(p->file_in); }
    }
  }
}

int main(void) 
{
  while(1)
  {
    printf("$ ");
    if (fgets(cmd_line, 2000, stdin) == NULL) { puts("INPUT_ERROR"); }
    job* j = parse_cmd();
    run(j);
  }
  return 0;
}
